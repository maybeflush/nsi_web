let total = 0;
let n = 0;
let meilleur=0;
function setup() {
  createCanvas(400, 400);
  background(255);
  rectMode(RADIUS);// Set fill to gray
  x_c = width / 2;
  y_c = height / 2;
  rect(x_c, y_c, 100, 100); 
  circle(x_c, y_c, 200)

}

function draw() {
  x = random(100, 300);
  y = random(100, 300);
  
  n = n +1;
  if (dist(x, y, x_c, y_c) <= 100) {
    total += 1;
    stroke(255, 0, 100);
    }
  point(x, y);
  stroke(0,0,255);
  textSize(32);
  fill(50);
  if (abs(4 * total / n-PI)<abs(meilleur-PI)){
    meilleur=4 * total / n;
    text("Pi = ",10,50);
    if(abs(4 * total / n-PI)<1/10000){
      
      text("Pi = "+str(meilleur),10,50);
      textSize(16);
      let t ="Précision demandée : 0,0001 --" + str(n) + " itérations";
      text(t, 10, 70);
      noLoop();
    }
  }
  

}